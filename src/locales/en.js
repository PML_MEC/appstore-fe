/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import enLocale from 'element-ui/lib/locale/lang/en'
const en = {
  nav: {
    store: 'Store',
    docs: 'Docs',
    myApp: 'My App',
    about: 'About',
    logout: 'Logout',
    appstore: 'Appstore'
  },
  common: {
    bannerTitle: 'One-step shop for developers',
    bannerWord: 'Provide developers with convenient development plugins to help developers develop and test apps faster',
    type: 'Type',
    video: 'Video',
    game: 'Game',
    VideoSurveilance: 'Video Surveilance',
    AR: 'AR/VR',
    api: 'API',
    sdk: 'SDK',
    MEP: 'MEP',
    Security: 'Security',
    blockchain: 'Blockchain',
    smartDevice: 'SmartDevice',
    IOT: 'Internet of Things',
    bigdata: 'BigData',
    calculate: 'Calculate',
    all: 'All',
    affinity: 'Affinity',
    GPU: 'GPU',
    MEN: 'MEM',
    CPU: 'CPU',
    AI: 'AI',
    industry: 'INDUTRY',
    smartCampus: 'Smart Campus',
    supermarket: 'Supermarket',
    industrialManufacturing: 'Industrial Manufacturing',
    logistics: 'Transportation and logistics',
    hydroelectricity: 'Hydroelectricity',
    games: 'Games and competitions',
    openSource: 'Open Source',
    else: 'Else',
    appName: 'App Name',
    version: 'Version',
    size: 'Size',
    provider: 'Provider',
    contact: 'Contact Information',
    operation: 'Operation',
    detail: 'Detail',
    delete: 'Delete',
    cancel: 'Cancel',
    confirm: 'Confirm',
    detect: 'Detect',
    download: 'Download',
    applicationName: 'Application Name',
    description: 'Description',
    userName: 'Uploder',
    getCaptcha: ' Get Captcha ',
    inputCaptcha: 'Please enter the Captcha code',
    choose: 'Please Choose'
  },
  promptMessage: {
    operationFailed: 'The operation failure!',
    modifySuccess: 'Modify the success!',
    modifyFail: 'Modify the failure',
    fileNotSupport: 'The file format does not support  or empty content!',
    deleteSuccess: 'Delete the success!',
    fileNotContent: 'The file format does not support  or empty content!',
    canOnlyUpload: 'Can only upload .',
    files: ' files.',
    uploadPicture: 'Can only upload .png files.',
    uploadPackageFile: 'You need to upload package file first!',
    uploadIconFile: 'You need to upload icon file or select icon first!',
    completedTest: 'You have completed the test, please click to view the report',
    uploadSuccess: 'Upload success!',
    uploadFailed: 'Upload failure!',
    checkName: 'Authentication Failure, please recheck the User Name or Password.',
    repeatEnter: 'The input information is wrong, please re-enter it.',
    userResistedSuccess: 'User is registered successfully!!',
    userResistedFaile: 'Failed to register user',
    deletePrompt: 'This operation will delete the app permanently. Do you want to continue?',
    prompt: 'Prompt',
    checkUploadProgress: 'Check upload progress immediately?',
    browserAdvise: 'For sure some functions work well, please use Chrome',
    userNameIsExists: 'The user name has been registered, please change a new one.',
    phoneNumIsExists: 'The phone number is registered.',
    subCommentFail: 'Comment submission failed.',
    getCommentFail: 'Failed to get comment.',
    getAppFail: 'Failed to get App data.',
    getTaskListFail: 'Failed to get test task.',
    getMyAppFail: 'Failed to get my App data.',
    verifyNamePhoneFail: 'Failed to verify user name or phone number.',
    registerFail: 'Registration failed.',
    getCaptchaSuccess: 'The verification code has been sent to your mobile phone, please check.',
    getCaptchaFail: 'Failed to obtain captcha.',
    confirmLogout: 'Are you sure to log out?',
    subCommentFailReason: 'Rating and content are required',
    contrastTime: 'The submission time cannot be earlier than the test end time!'
  },
  store: {
    advancedSearch: 'Advanced Search',
    clearAll: 'Delete',
    screening: 'Screening',
    sortBy: 'Sort By',
    most: 'Most',
    name: 'Name',
    score: 'Score',
    appPackageInfo: 'App Package Basic Information',
    createTime: 'Create Time',
    download: 'Download',
    comments: 'Comments',
    message: 'Message',
    postComment: 'Post a Comment',
    uploadApp: 'Upload App Package',
    appPackage: 'App Package',
    dragPackage: 'Drag App package files here orclick to upload',
    onlyCsar: 'Can only upload .csar files.',
    packageSizeLimit: 'no more than 200M',
    appIcon: 'App Icon',
    onlyImg: 'Can only upload .png file, or select the default App icon.',
    iconSizeLimit: 'no more than 2M.',
    userName: 'Commentator',
    time: 'Time',
    toBeAdded: 'To be added'
  },
  docs: {
    step1: 'Open source MEP platform',
    step1Intr: 'OpenMEC native installation deployment',
    step1Info1: 'Open source personnel access to the MEP platform open source code',
    step1Info2: 'Local installation deployment',
    step1Process: 'Platform select > capability select > toolchain installation/sample code generation',
    step2: 'Development process',
    step2Intr: 'Application development',
    step2Info1: 'Capture platform customization capabilities and integration specifications',
    step2Info2: 'Application development self - test and upload integration test',
    step2Process: 'Develop self - test > application package > integration test',
    step3: 'Open source APP warehouse',
    step3Intr: 'Application of release',
    step3Info1: 'Application release the application can be released to the open source APP repository after the integration test',
    step3Process: 'Application publishing > publishing commercial Store',
    step4: 'MEC management system',
    step4Intr: 'Application deployment',
    step4Info1: 'Applications deployed and put online in the APP warehouse will be released to the shelf of huawei business solutions',
    step4Info2: 'Through the MEC management system, automatically or manually sent to the MEC node',
    step4Process: 'MEC management system',
    updated: 'Updated',
    modify: 'Modify',
    share: 'Share',
    favorite: 'Favorite',
    docs: 'Docs'
  },
  myApp: {
    checkTest: 'Check Test',
    testStatus: 'Test Status',
    startTime: 'Starting Time',
    reset: 'Reset',
    inquire: 'Inquire',
    version: 'Version',
    endTime: 'End Time',
    operation: 'Operation',
    taskNumber: 'Task Number',
    checkReport: 'Check Report',
    upload: 'Upload',
    appName: 'Application Name',
    status: 'Status'
  },
  report: {
    version: 'Version',
    packageName: 'Package Name',
    startTime: 'Starting Time',
    endTime: 'End Time',
    status: 'Status',
    detail: 'Details',
    ticketNumber: 'Mission ticket number',
    baseInfo: 'Basic Information'
  },
  about: {
  },
  ...enLocale
}
export default en
