/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const INDUSTRY = [
  {
    label: ['智慧园区', 'Smart Campus'],
    value: ['智慧园区', 'Smart Campus'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['智慧商超', 'Supermarket'],
    value: ['智慧商超', 'Supermarket'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['工业制造', 'Industrial Manufacturing'],
    value: ['工业制造', 'Industrial Manufacturing'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['交通物流', 'Transportation and logistics'],
    value: ['交通物流', 'Transportation and logistics'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['水利', 'Hydroelectricity'],
    value: ['水利', 'Hydroelectricity'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['游戏竞技', 'Games and competitions'],
    value: ['游戏竞技', 'Games and competitions'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['开源', 'Open Source'],
    value: ['开源', 'Open Source'],
    selected: false,
    type: 'industry'
  },
  {
    label: ['其他', 'Else'],
    value: ['其他', 'Else'],
    selected: false,
    type: 'industry'
  }
]

const TYPES = [
  {
    label: ['视频应用', 'Video'],
    value: ['视频应用', 'Video'],
    selected: false,
    type: 'types'
  },
  {
    label: ['游戏', 'Game'],
    value: ['游戏', 'Game'],
    selected: false,
    type: 'types'
  },
  {
    label: ['视频监控', 'Video Surveilance'],
    value: ['视频监控', 'Video Surveilance'],
    selected: false,
    type: 'types'
  },
  {
    label: ['安全', 'Security'],
    value: ['安全', 'Security'],
    selected: false,
    type: 'types'
  },
  {
    label: ['区块链', 'Blockchain'],
    value: ['区块链', 'Blockchain'],
    selected: false,
    type: 'types'
  },
  {
    label: ['智能设备', 'SmartDevice'],
    value: ['智能设备', 'SmartDevice'],
    selected: false,
    type: 'types'
  },
  {
    label: ['物联网', 'Internet of Things'],
    value: ['物联网', 'Internet of Things'],
    selected: false,
    type: 'types'
  },
  {
    label: ['大数据', 'BigData'],
    value: ['大数据', 'BigData'],
    selected: false,
    type: 'types'
  },
  {
    label: ['AR/VR', 'AR/VR'],
    value: ['AR/VR', 'AR/VR'],
    selected: false,
    type: 'types'
  },
  {
    label: ['API', 'API'],
    value: ['API', 'API'],
    selected: false,
    type: 'types'
  },
  {
    label: ['SDK', 'SDK'],
    value: ['SDK', 'SDK'],
    selected: false,
    type: 'types'
  },
  {
    label: ['MEP', 'MEP'],
    value: ['MEP', 'MEP'],
    selected: false,
    type: 'types'
  }
]

const AFFINITY = [
  {
    label: 'x86',
    value: 'x86',
    selected: false,
    type: 'affinity'
  },
  {
    label: 'ARM64',
    value: 'ARM64',
    selected: false,
    type: 'affinity'
  },
  {
    label: 'ARM32',
    value: 'ARM32',
    selected: false,
    type: 'affinity'
  },
  {
    label: 'GPU',
    value: 'GPU',
    selected: false,
    type: 'affinity'
  },
  {
    label: 'NPU',
    value: 'NPU',
    selected: false,
    type: 'affinity'
  }
]

const SORT_BY = [
  {
    label: 'store.most',
    value: 'Most',
    selected: false,
    type: 'sortBy'
  },
  {
    label: 'store.name',
    value: 'Name',
    selected: false,
    type: 'sortBy'
  },
  {
    label: 'store.score',
    value: 'Score',
    selected: false,
    type: 'sortBy'
  }
]

export {
  TYPES, AFFINITY, SORT_BY, INDUSTRY
}
